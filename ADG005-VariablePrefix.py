"""Lint rule class to test if variables have the proper prefix
"""

import re
import os
import typing
import ansiblelint.rules

class VariablePrefix(ansiblelint.rules.AnsibleLintRule):
    """
    Rule class to test if variables have the proper prefix
    """
    id: str = 'ADG005-variable-prefix'
    shortdesc: str = 'variable does not have the proper prefix'
    description: str = """Only variables with the rolename as prefix are allowed"""
    severity: str = "HIGH"
    tags: typing.List[str] = [id, 'variables']
    needs_raw_task = True

    def matchtask(self, task, file):
        results = []

        if task['action']['__ansible_module__'] == 'template':
            role_folder = os.path.dirname(file['path']).replace('/tasks','')
            role_name: str = role_folder.split('/')[-1]
            vars_files = ['defaults/main.yml', 'vars/main.yml']

            for vars_file in vars_files:
                file_path = role_folder + "/" + vars_file
                regex = "(-{3}|\s{2}|#|" + role_name + "_|\r|\n)"
                with open(file_path, 'r') as file:
                    lines = file.readlines()

                    for line in lines:
                        variable = str(line.strip().split(':',1)[0])
                        if not re.match(regex, line):
                            results.append(variable)

            return(f'Variables: {str(results)} do not start with "{role_name}".')
            
        return False
