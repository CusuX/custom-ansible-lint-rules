"""Lint rule class to test if variables contain spaces
"""

import re
import os
import typing
import ansiblelint.rules

class VariableCase(ansiblelint.rules.AnsibleLintRule):
    """
    Rule class to test if variables contain spaces
    """
    id: str = 'ADG013-variable-spaces'
    shortdesc: str = 'variable contains a space'
    description: str = """Only variables without spaces are allowed"""
    severity: str = "HIGH"
    tags: typing.List[str] = [id, 'variables']
    needs_raw_task = True

    def matchtask(self, task, file):
        results = []

        if task['action']['__ansible_module__'] == 'template':
            role_folder = os.path.dirname(file['path']).replace('/tasks','')
            vars_files = ['defaults/main.yml', 'vars/main.yml']

            for vars_file in vars_files:
                file_path = role_folder + "/" + vars_file
                regex = "(-{3}|\s{2}|#|\r|\n)"
                with open(file_path, 'r') as file:
                    lines = file.readlines()

                    for line in lines:
                        variable = str(line.strip().split(':',1)[0])
                        if not re.match(regex, line):
                            if re.search('\s', variable):
                                results.append(variable)

            return(f'Variable(s): {str(results)} contain(s) a space.')
            
        return False
