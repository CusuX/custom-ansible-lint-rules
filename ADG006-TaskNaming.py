"""Lint rule class to test if tasks are named
"""

import typing
import ansiblelint.rules

class TaskNaming(ansiblelint.rules.AnsibleLintRule):
    """
    Rule class to test if tasked are named
    """
    id: str = 'ADG006-task-naming'
    shortdesc: str = 'task is not named'
    description: str = """Only named tasks are allowed"""
    severity: str = "HIGH"
    tags: typing.List[str] = [id, 'tasks']
    needs_raw_task = True

    def matchtask(self, task, file):
        module = task['action']['__ansible_module__']
        if task.name == None:
            return f'Task with module "{module}" isn\'t named'
        return False
