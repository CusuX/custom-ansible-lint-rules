"""Lint rule class to test if template files have the `{{ ansible_managed }}` variable on line 1
"""

import os
import typing
from ansiblelint.file_utils import Lintable
import ansiblelint.rules

class TemplateFilenames(ansiblelint.rules.AnsibleLintRule):
    """
    Rule class to test if template files have the `{{ ansible_managed }}` variable on line 1
    """
    id: str = 'ADG004-template-tracking'
    shortdesc: str = """template file does not have the `{{ ansible_managed }}` variable on line 1"""
    description: str = """Only template files with the `{{ ansible_managed }}` variable on line 1 are allowed"""
    severity: str = "HIGH"
    tags: typing.List[str] = [id, 'templates', 'tracking']
    needs_raw_task = True

    def matchtask(self, task, file: Lintable):
        file_dir = file.dir

        if "tasks" in file_dir:
            template_dir: str = file_dir.partition("/tasks")[0] + "/templates"

        if "handlers" in file_dir:
            template_dir: str = file_dir.partition("/handlers")[0] + "/templates"

        for (template_dir, dir_names, file_names) in os.walk(template_dir):
            template_files = file_names

        for template_file in template_files:
            f = open(template_dir + "/" + template_file)
            data = f.read()
            first_line = data.split('\n', 1)[0]
            f.close()

            if not "{{ ansible_managed }}" in first_line:
                return(f'Template file "{template_file}" must have the "ansible_managed" variable on line 1.')

        return False