"""Lint rule class to test if variables are lowercase
"""

import re
import os
import typing
import ansiblelint.rules

class VariableCase(ansiblelint.rules.AnsibleLintRule):
    """
    Rule class to test if variables are lowercase
    """
    id: str = 'ADG012-variable-case'
    shortdesc: str = 'variable is not lowercase'
    description: str = """Only lowercase variables are allowed"""
    severity: str = "HIGH"
    tags: typing.List[str] = [id, 'variables']
    needs_raw_task = True

    def matchtask(self, task, file):
        results = []

        if task['action']['__ansible_module__'] == 'template':
            role_folder = os.path.dirname(file['path']).replace('/tasks','')
            vars_files = ['defaults/main.yml', 'vars/main.yml']

            for vars_file in vars_files:
                file_path = role_folder + "/" + vars_file
                regex = "(-{3}|\s{2}|#|\r|\n)"
                with open(file_path, 'r') as file:
                    lines = file.readlines()

                    for line in lines:
                        variable = str(line.strip().split(':',1)[0])
                        if not re.match(regex, line):
                            if not variable.islower():
                                results.append(variable)

            return(f'Variables: {str(results)} are now lowercase.')
            
        return False
