# Custom ansible-lint rules

This repository contains custom rules for `ansible-lint` in accordance to my Ansible Development Guidelines.

## Rules

| Rule       | Description                                                      |
|------------|------------------------------------------------------------------|
| **ADG003** | Test template source variable for use of the `.j2` extension     |
| **ADG004** | Test if template file has `{{ ansible_managed }}` on first line  |
| **ADG005** | Test to check if variables start with role name                  |
| **ADG006** | Test tasks to check if they are named                            |
| **ADG007** | Test main task file for disallowed tasks                         |
| **ADG008** | Test main task file tasks for tags                               |
| **ADG009** | Test tags for naming convention                                  |
| **ADG010** | Test the use of unsupported Ansible Content Collections          |
| **ADG011** | Test the use of Fully Qualified Collection Names                 |
| **ADG012** | Test if variables are declared in lowercase                      |
| **ADG013** | Test if variables contain a space                                |
| **ADG015** | Test if empty variables contain a default value                  |