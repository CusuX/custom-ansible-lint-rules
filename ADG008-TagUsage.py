"""Lint rule class to test if main task file tasks use tags
"""

import typing
import ansible
import ansiblelint.rules

class TagUsage(ansiblelint.rules.AnsibleLintRule):
    """
    Rule class to test if main task file tasks use tags
    """
    id: str = 'ADG008-tag-usage'
    shortdesc: str = 'main task file tasks has tags'
    description: str = """Tasks in main task file must have tags"""
    severity: str = "HIGH"
    tags: typing.List[str] = [id, 'tags']
    needs_raw_task = True

    def matchtask(self, task, file):
        main_task_file: str = 'tasks/main.yml'
        if main_task_file in file.filename:
            for tag in task['tags']:
                if not isinstance(tag, ansible.parsing.yaml.objects.AnsibleUnicode):
                    return(f'Tag syntax in "{main_task_file}" should be of type list')

        return False
