"""Lint rule class to test if Fully Qualified Collection Names are used for modules
"""

import typing
import ansiblelint.rules

class FullyQualifiedCollectionNames(ansiblelint.rules.AnsibleLintRule):
    """
    Rule class to test if Fully Qualified Collection Names are used for modules
    """
    id: str = 'ADG011-fqcn'
    shortdesc: str = 'FQCN is used'
    description: str = """Modules must have Fully Qualified Collection names"""
    severity: str = "HIGH"
    tags: typing.List[str] = [id, 'modules']
    needs_raw_task = True

    def matchtask(self, task, file):
        module = task['action']['__ansible_module__']
        fqcn_full = task['action']['__ansible_module_original__']
        fqcn_full_list = fqcn_full.split('.')
        fqcn = '.'.join(fqcn_full_list[:2])
        
        if module == fqcn:
            return(f'Module "{module}" does not have a Fully Qualified Collection Name')

        return False
