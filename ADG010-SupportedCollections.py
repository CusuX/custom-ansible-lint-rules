"""Lint rule class to test if unsupported Ansible Content Collections are used
"""

import typing
import ansiblelint.rules

if typing.TYPE_CHECKING:
    from typing import Optional
    from ansiblelint.file_utils import Lintable

ID: str = 'supported_collections'
C_SUPPORTED_COLLECTIONS: str = 'supported'

SUPPORTED_COLLECTIONS: typing.FrozenSet[str] = frozenset("""
amazon.aws
ansible.builtin
ansible.controller
ansible.netcommon
ansible.network
ansible.posix
ansible.receptor
ansible.scm
ansible.snmp
ansible.security
ansible.utils
ansible.windows
ansible.yang
arista.cvp
arista.avd
arista.eos
arubanetworks.aoscx
avantra.core
check_point.gaia
check_point.mgmt
chocolatey.chocolatey
cisco.aci
cisco.asa
cisco.dnac
cisco.dcnm
cisco.ios
cisco.iosxr
cisco.ise
cisco.mso
cisco.nxos
citrix.adc
cloud.common
cloud.terraform
cohesity.dataprotect
confluent.platform
crowdstrike.falcon
cyberark.conjur
cyberark.pas
delinea.core
dellemc.enterprise_sonic
dellemc.isilon
dellemc.powerflex
dellemc.powermax
dellemc.powerscale
dellemc.powerstore
dellemc.unity
dellemc.vplex
dellemc.openmanage
dynatrace_innovationlab.dynatrace_collection
dynatrace.oneagent_deploy
f5networks.f5os
f5networks.f5_modules
f5networks.f5_bigip
frr.frr
fortinet.fortianalyzer
fortinet.fortimanager
fortinet.fortios
fortinet.fortiswitch
google.cloud
hpe.ilo
hpe.oneview
ibm.power_aix
ibm.power_hmc
ibm.power_ibmi
ibm.isam
ibm.power_vios
ibm.spectrum_virtualize
ibm.qradar
ibm.ibm_zhmc
ibm.ibm_zosmf
ibm.ibm_zos_core
ibm.ibm_zos_ims
ibm.ibm_zos_cics
ibm.ibm_zos_sysauto
infinidat.infinibox
infoblox.nios_modules
inspur.ispim
inspur.sm
junipernetworks.junos
kubernetes.core
kong.kong
logicmonitor.integration
azure.azcollection
microsoft.ad
microsoft.sql
monitorapp.monitorapp_waf
netapp.aws
netapp.azure
netapp.cloudmanager
netapp.elementsw
netapp.ontap
netapp.um_info
nginxinc.nginx_core
nutanix.ncp
nvidia.cumulus_linux_roles
onepassword.connect
opensvc.cluster
openvswitch.openvswitch
paloaltonetworks.panos
phoenixnap.bmc
purestorage.flasharray
purestorage.flashblade
purestorage.fusion
purestorage.pure1
redhat.amq_broker
redhat.insights
redhat.openshift
redhat.data_grid
redhat.eap
redhat.jws
redhat.redhat_csp_download
redhat.sso
redhat.rhel_idm
redhat.rhel_system_roles
redhat.rhv
redhat.runtimes_common
redhat.satellite
redhat.satellite_operations
sap.rhel
redhat.sap_install
sap.sap_operations
scale_computing.hypercore
seiko.smartcs
sensu.sensu_go
servericenow.itsm
splunk.es
styra.opa
trendmicro.deepsec
virsec.vsp
vmware.alb
vmware.vmware_rest
vyos.vyos
wti.remote
zabbix.zabbix
""".split())

class SupportedCollections(ansiblelint.rules.AnsibleLintRule):
    """
    Rule class to test if unsupported Ansible Content Collections are used
    """
    id: str = 'ADG010-supported-collections'
    shortdesc: str = 'unsupported Ansible Content Collection found'
    description: str = """Only supported Ansible Content Collections
    are allowed.
    """
    severity: str = "MEDIUM"
    tags: typing.List[str] = [id, 'collections']
    needs_raw_task = True

    def supported_collections(self):
        """
        .. seealso:: rules.DebugRule.DebugRule.enabled
        """
        supported = self.get_config(C_SUPPORTED_COLLECTIONS)
        if supported:
            return frozenset(supported)

        return SUPPORTED_COLLECTIONS

    def matchtask(self, task: typing.Dict[str, typing.Any],
                  file: 'Optional[Lintable]' = None
                  ) -> typing.Union[bool, str]:
        """
        .. seealso:: ansiblelint.rules.AnsibleLintRule.matchtasks
        """
        
        module = task['action']['__ansible_module__']
        fqcn_full = task['action']['__ansible_module_original__']
        fqcn_full_list = fqcn_full.split('.')
        fqcn = '.'.join(fqcn_full_list[:2])
        if fqcn != module:
            if not fqcn in self.supported_collections():
                return f'{self.shortdesc}: {fqcn}'

        return False
