"""Lint rule class to test if tags have proper naming convention
"""

import typing
from ansiblelint.file_utils import Lintable
import ansiblelint.rules

class TagUsage(ansiblelint.rules.AnsibleLintRule):
    """
    Rule class to test if main task file tasks use tags
    """
    id: str = 'ADG009-tag-naming'
    shortdesc: str = 'tags naming convention'
    description: str = """Tags must start with 'tag_rolename_'"""
    severity: str = "HIGH"
    tags: typing.List[str] = [id, 'tags']
    needs_raw_task = True

    def matchtask(self, task, file: Lintable):
        file_dir = file.dir

        if "tasks" in file_dir:
            rolename: str = file_dir.partition("/tasks")[0].split('/')[-1]

        if "handlers" in file_dir:
            rolename: str = file_dir.partition("/handlers")[0].split('/')[-1]

        tag_prefix: str = "tag_" + rolename + "_"

        if 'tags' in task.raw_task:
            tags = task.raw_task['tags']

            if 'AnsibleSequence' in str(type(tags)):
                for tag in tags:
                    if not tag == 'debug':
                        if not tag == 'never':
                            if not tag.startswith(tag_prefix):
                                return(f'Tag "{tag}" must start with "{tag_prefix}"')

            if 'AnsibleUnicode' in str(type(tags)):
                if not tags == 'debug':
                    if not tags == 'never':
                        if not tags.startswith(tag_prefix):
                            return(f'Tag "{tags}" must start with "{tag_prefix}"')

        return False
