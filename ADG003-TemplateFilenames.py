"""Lint rule class to test if template source variables have the `.j2` extension
"""

import os
import typing
import ansiblelint.rules

class TemplateFilenames(ansiblelint.rules.AnsibleLintRule):
    """
    Rule class to test if template sources have the `.j2` extension
    """
    id: str = 'ADG003-template-source-variables'
    shortdesc: str = 'template source variable does not have the `.j2` extension'
    description: str = """Only template source variables with the `.j2` extension are allowed"""
    severity: str = "HIGH"
    tags: typing.List[str] = [id, 'templates']
    needs_raw_task = True

    def matchtask(self, task, file):
        if task['action']['__ansible_module__'] == 'template':
            template_var: str = task['action']['src']
            if '{{' in template_var:
                search_var: str = template_var.replace('{', '').replace('}','').strip()
                role_folder = os.path.dirname(file['path'])
                vars_files = ['defaults/main.yml', 'vars/main.yml']

                for vars_file in vars_files:
                    file_path = role_folder + "/../" + vars_file
                    with open(file_path, 'r') as file:
                        lines = file.readlines()

                        for line_number, line in enumerate(lines, start=1):
                            if search_var in line:
                                if not '.j2' in line:
                                    return f'Template source variable "{template_var}" with value "{line.split(":")[1].strip()}" as defined in "{vars_file}" on line: {line_number}, does not end with .j2'

            if not '{{' in template_var:
                if not template_var.endswith('.j2'):
                    return f'Template source variable "{template_var}" does not end with .j2'

        return False
