"""Lint rule class to test if main task file only has includes or imports
"""

import typing
import ansiblelint.rules

class MainTaskFile(ansiblelint.rules.AnsibleLintRule):
    """
    Rule class to test if main task file only has includes of imports
    """
    id: str = 'ADG007-main-task-file'
    shortdesc: str = 'main task file has only imports and includes'
    description: str = """Only imports and includes are allowed"""
    severity: str = "HIGH"
    tags: typing.List[str] = [id, 'tasks']
    needs_raw_task = True

    def matchtask(self, task, file):
        main_task_file: str = 'tasks/main.yml'
        if main_task_file in file.filename:
            module = task['action']['__ansible_module__']
            if module not in ("import_tasks","include_tasks"):
                return f'Main task file has disallowed task: "{module}"'

        return False
